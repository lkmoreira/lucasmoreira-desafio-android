package br.com.gitlist.business.activity.base;

import android.net.ConnectivityManager;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.AnimationRes;

import br.com.gitlist.R;
import br.com.gitlist.business.view.base.BaseView;
import br.com.gitlist.rest.request.CallbackConnection;
import br.com.gitlist.utils.NetworkUtils;

/**
 * Created by lucas.moreira on 26/11/16.
 */
@EActivity
public class BaseActivity extends AppCompatActivity implements BaseView {

    @AnimationRes
    protected Animation slideDown, slideUp;

    @ViewById
    protected ViewGroup bottomMessageContainer, loadingContainer;

    @ViewById
    protected TextView offlineContainer;
    protected boolean hasNetworkConnection = true;

    @AfterViews
    protected void baseAfterViews() {
        setupAnimations();
        afterViews();
    }


    /**
     * Method for loading afterViews methods in the all activities for not
     * duplicate calls
     */
    protected void afterViews() {
    }

    protected void reloadAfterViews() {
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        View view = getLayoutInflater().inflate(R.layout.main_layout, null);
        RelativeLayout content = (RelativeLayout) view.findViewById(R.id.content);
        View child = getLayoutInflater().inflate(layoutResID, null);
        child.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        content.addView(child);
        super.setContentView(view);
    }

    @Override
    public void showProgress() {
        if (offlineContainer.getVisibility() == View.VISIBLE)
            return;

        loadingContainer.setVisibility(View.VISIBLE);
        bottomMessageContainer.startAnimation(slideUp);
    }

    @Override
    public void hideProgress() {
        if (offlineContainer.getVisibility() == View.VISIBLE)
            return;

        bottomMessageContainer.startAnimation(slideDown);
    }

    @Override
    public void showOfflineProgress() {
        if (loadingContainer.getVisibility() == View.VISIBLE)
            loadingContainer.setVisibility(View.GONE);

        offlineContainer.setVisibility(View.VISIBLE);
        bottomMessageContainer.startAnimation(slideUp);
    }

    @Override
    public void hideOfflineProgress() {
        bottomMessageContainer.startAnimation(slideDown);
    }

    private void setupAnimations() {
        slideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                bottomMessageContainer.setVisibility(View.VISIBLE);
                bottomMessageContainer.bringToFront();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        slideDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                loadingContainer.setVisibility(View.GONE);
                offlineContainer.setVisibility(View.GONE);
                bottomMessageContainer.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Receiver(actions = ConnectivityManager.CONNECTIVITY_ACTION, registerAt = Receiver.RegisterAt.OnResumeOnPause)
    protected void changeConnection() {
        NetworkUtils.executeHasInternet(new CallbackConnection() {
            @Override
            public void onConnection() {
                setHasNetworkConnection(true);
                hideOfflineProgress();
            }

            @Override
            public void onOffline() {
                setHasNetworkConnection(false);
                showOfflineProgress();
            }
        }, this);
        onConnectionChanged();
    }

    public void checkIfHasInternet(final boolean loadItems) {
        NetworkUtils.executeHasInternet(new CallbackConnection() {
            @Override
            public void onConnection() {
                setHasNetworkConnection(true);
                hideOfflineProgress();
                if (loadItems)
                    loadItems(true);
            }

            @Override
            public void onOffline() {
                setHasNetworkConnection(false);
                showOfflineProgress();
                if (loadItems)
                    loadItems(false);
            }
        }, this);

    }

    public void loadItems(boolean isUpdate) {
    }

    public void onConnectionChanged() {
    }

    public boolean isHasNetworkConnection() {
        return hasNetworkConnection;
    }

    public void setHasNetworkConnection(boolean hasNetworkConnection) {
        this.hasNetworkConnection = hasNetworkConnection;
    }

}
