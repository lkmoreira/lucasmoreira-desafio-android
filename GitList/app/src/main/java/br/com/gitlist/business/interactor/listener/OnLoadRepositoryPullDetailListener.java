package br.com.gitlist.business.interactor.listener;

import java.util.List;

import br.com.gitlist.business.model.RepositoryPullDetail;

/**
 * Created by lukas on 26/11/16.
 */

public interface OnLoadRepositoryPullDetailListener {

    void onSuccess();

    void onSuccess(List<RepositoryPullDetail> repositories, boolean isUpdate);

    void onError();
}
