package br.com.gitlist.utils;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.util.LruCache;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import br.com.gitlist.ui.customview.CircleNetworkImageView;

/**
 * Created by lucas.moreira on 26/11/16.
 */

public class VolleySingleton {

    @SuppressLint("StaticFieldLeak")
    private static VolleySingleton mInstance = null;

    private RequestQueue mRequestQueue;

    private ImageLoader mImageLoader;

    @SuppressLint("StaticFieldLeak")
    private static Context mCtx;

    private VolleySingleton(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
        mImageLoader = new ImageLoader(this.mRequestQueue, new ImageLoader.ImageCache() {

            ActivityManager am = (ActivityManager) mCtx.getSystemService(
                    Context.ACTIVITY_SERVICE);
            final int maxKb = am.getMemoryClass() * 1024 * 1024;
            final int limitKb = maxKb / 8; // 1/8th of total ram

            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(limitKb) {
                @Override
                protected int sizeOf(String key, Bitmap bitmap) {
                    return bitmap.getByteCount() / 1024;
                }
            };

            public void putBitmap(String url, Bitmap bitmap) {
                Log.d("mCache", "[salvando cache=" + (bitmap != null ? sizeOf(bitmap) : null) + "]");
                mCache.put(url, bitmap);

            }

            public Bitmap getBitmap(String url) {
                Log.d("mCache", "[get cache=" + mCache.get(url) + "]");
                if (mCache.get(url) != null)
                    return mCache.get(url);

                mCache.remove(url);
                return null;
            }
        });
    }

    public static VolleySingleton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new VolleySingleton(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mCtx);
        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        return this.mImageLoader;
    }

    public static void loadImage(String url, NetworkImageView imageView, Context context) {
        ImageLoader loader = VolleySingleton.getInstance(context).getImageLoader();
        imageView.setImageUrl(url, loader);
    }

    public static void loadImage(String url, NetworkImageView imageView, Context context,
                                 final int defaultImageResId, final int errorImageResId) {
        ImageLoader loader = VolleySingleton.getInstance(context).getImageLoader();
        loader.get(url, ImageLoader.getImageListener(imageView,
                defaultImageResId, errorImageResId));
        imageView.setImageUrl(url, loader);
    }

    public static void loadImage(String url, NetworkImageView imageView, Context context,final int errorImageResId) {
        ImageLoader loader = VolleySingleton.getInstance(context).getImageLoader();
        loader.get(url, ImageLoader.getImageListener(imageView,
                0, errorImageResId));
        imageView.setImageUrl(url, loader);
    }



    protected int sizeOf(Bitmap data) {
        if (data == null)
            return 0;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return (data.getRowBytes() * data.getHeight());
        } else {
            return data.getByteCount();
        }
    }

}
