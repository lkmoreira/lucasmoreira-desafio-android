package br.com.gitlist.business.presenter.impl;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import br.com.gitlist.business.interactor.RepositoryInteractor;
import br.com.gitlist.business.interactor.impl.RepositoryInteractorImpl;
import br.com.gitlist.business.interactor.listener.OnLoadRepositoryListener;
import br.com.gitlist.business.model.Repository;
import br.com.gitlist.business.presenter.RepositoryPresenter;
import br.com.gitlist.business.view.RepositoryView;

/**
 * Created by lucas.moreira on 26/11/16.
 *
 */
@EBean
public class RepositoryPresenterImpl implements RepositoryPresenter, OnLoadRepositoryListener {


    private RepositoryView repositoryView;

    @Bean(RepositoryInteractorImpl.class)
    protected RepositoryInteractor repositoryInteractor;

    @Override
    public void onSuccess() {

    }

    @Override
    public void onSuccess(List<Repository> repositories, boolean isUpdate) {
        repositoryView.populateList(repositories, isUpdate);
    }

    @Override
    public void onError() {
        repositoryView.populateList(new ArrayList<Repository>(), false);
    }

    @Override
    public void findRepositories(int page, boolean isUpdate) {
        repositoryInteractor.findRepositories(this, page, isUpdate);
    }

    @Override
    public void setView(RepositoryView view) {
        this.repositoryView = view;
    }
}
