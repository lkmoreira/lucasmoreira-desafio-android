package br.com.gitlist.ui.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.gitlist.R;
import br.com.gitlist.business.activity.RepositoryPullDetailActivity;
import br.com.gitlist.business.activity.RepositoryPullDetailActivity_;
import br.com.gitlist.business.activity.base.BaseActivity;
import br.com.gitlist.business.model.Repository;
import br.com.gitlist.business.model.RepositoryPullDetail;
import br.com.gitlist.utils.VolleySingleton;


/**
 * Created by lucas.moreira on 26/11/16.
 *
 */
@EViewGroup(R.layout.repository_item)
public class RepositoryItemView extends LinearLayout {

    @ViewById
    protected TextView repositoryName, repositoryDescription, forkCount, starCount, username, fullName;

    @ViewById
    protected CustomCircleImageView thumbnail;

    private BaseActivity activity;
    private Repository repository;

    public RepositoryItemView(Context context) {
        super(context);
        activity = (BaseActivity) context;
    }

    public RepositoryItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        activity = (BaseActivity) context;
    }

    public RepositoryItemView bind(final Repository repository) {
        this.repository = repository;

        repositoryName.setText(repository.getName());
        repositoryDescription.setText(repository.getDescription());
        forkCount.setText(String.valueOf(repository.getForks()));
        starCount.setText(String.valueOf(repository.getStargazersCount()));
        username.setText(repository.getOwner().getLogin());
        fullName.setText(repository.getFullName());
        thumbnail.displayImage(repository.getOwner().getAvatarUrl(), activity, R.drawable.account_circle);

        return this;
    }

    @Click(R.id.repository_item)
    void showDetails() {
        RepositoryPullDetailActivity_.intent(activity).repository(repository).start();
        activity.overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }


}
