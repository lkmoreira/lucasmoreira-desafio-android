package br.com.gitlist.business.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.NonConfigurationInstance;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.gitlist.R;
import br.com.gitlist.business.activity.base.BaseActivity;
import br.com.gitlist.business.model.Repository;
import br.com.gitlist.business.model.RepositoryPullDetail;
import br.com.gitlist.business.presenter.RepositoryPullDetailPresenter;
import br.com.gitlist.business.presenter.impl.RepositoryPullDetailPresenterImpl;
import br.com.gitlist.business.view.RepositoryPullDetailView;
import br.com.gitlist.ui.adapter.RepositoryAdapter;
import br.com.gitlist.ui.adapter.RepositoryPullDetailsAdapter;

@EActivity(R.layout.activity_repository)
public class RepositoryPullDetailActivity extends BaseActivity implements RepositoryPullDetailView {

    @ViewById
    protected RecyclerView items;

    @ViewById
    protected SwipeRefreshLayout swipeRefreshContainer;

    @ViewById
    protected RelativeLayout emptyView;

    @Bean(RepositoryPullDetailPresenterImpl.class)
    protected RepositoryPullDetailPresenter presenter;

    @Extra
    protected Repository repository;

    @Bean
    @NonConfigurationInstance
    protected RepositoryPullDetailsAdapter repositoryAdapter;

    @AfterInject
    protected void afterInject() {
        checkIfHasInternet(true);
    }

    @Override
    protected void afterViews() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.txt_pull_details_activity_title, repository.getName()));

        setupRecyclerView();
        showProgress();

        presenter.setView(this);
        presenter.findRepositoriesDetails(false, repository.getOwner().getLogin(), repository.getName());

        swipeRefreshContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadItems(true);
            }
        });
    }

    @Override
    public void setupRecyclerView() {
        if (items == null)
            return;

        final GridLayoutManager layoutManager = new GridLayoutManager(this, this.getResources().getInteger(R.integer.num_of_columns));
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.scrollToPosition(0);
        items.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        items.setAdapter(repositoryAdapter);
        items.setLayoutManager(layoutManager);

    }

    @Override
    public void populateList(List<RepositoryPullDetail> value, boolean isUpdate) {
        if (value.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            items.setVisibility(View.GONE);
            return;
        } else {
            items.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }

        repositoryAdapter.setData(value, isUpdate);

        if (swipeRefreshContainer.isRefreshing())
            swipeRefreshContainer.setRefreshing(false);

        hideProgress();
    }

    @Override
    public void loadMore() {
        if (hasNetworkConnection) {
            showProgress();
            presenter.findRepositoriesDetails(true, repository.getOwner().getLogin(), repository.getName());
        }
    }

    @Override
    public void loadItems(boolean isUpdate) {
        if (hasNetworkConnection) {
            showProgress();
            presenter.findRepositoriesDetails(true, repository.getOwner().getLogin(), repository.getName());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_right);
    }
}
