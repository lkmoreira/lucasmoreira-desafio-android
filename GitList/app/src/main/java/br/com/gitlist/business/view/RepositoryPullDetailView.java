package br.com.gitlist.business.view;

import java.util.List;

import br.com.gitlist.business.model.Repository;
import br.com.gitlist.business.model.RepositoryPullDetail;

public interface RepositoryPullDetailView {

    void setupRecyclerView();

    void populateList(List<RepositoryPullDetail> value, boolean isUpdate);

    void loadMore();
}