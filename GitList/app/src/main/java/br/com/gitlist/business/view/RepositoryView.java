package br.com.gitlist.business.view;

import java.util.List;

import br.com.gitlist.business.model.Repository;

public interface RepositoryView {

    void setupRecyclerView();

    void populateList(List<Repository> value, boolean isUpdate);

    void loadMore();
}