package br.com.gitlist.business.interactor.base;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import br.com.gitlist.rest.Rest;

/**
 * Created by lucas.moreira on 26/11/16.
 *
 */
@EBean
public class BaseInteractor {

    @Bean
    protected Rest rest;

}
