package br.com.gitlist.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.IOException;

import br.com.gitlist.rest.request.CallbackConnection;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import rx.Single;
import rx.SingleSubscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by lucas.moreira on 27/11/16.
 */
public class NetworkUtils {

    public static void executeHasInternet(final CallbackConnection callbackConnection, final Context context) {
        Single.create(new Single.OnSubscribe<Boolean>() {
            @Override
            public void call(SingleSubscriber singleSubscriber) {
                try {
                    boolean hasInternet = hasInternet(context);
                    singleSubscriber.onSuccess(hasInternet);
                } catch (IOException e) {
                    singleSubscriber.onError(e.getCause());
                }
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean hasConnection) {
                        if (hasConnection)
                            callbackConnection.onConnection();
                        else
                            callbackConnection.onOffline();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        callbackConnection.onOffline();
                    }
                });

    }

    public static boolean hasInternet(Context context) throws IOException {
        if (isNetworkAvailable(context)) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url("http://www.google.com/")
                    .build();
            Response response = client.newCall(request).execute();
            Log.d("Networkutils", "ResponseCode: " + response.code());
            return response.code() < 300;
        }
        return false;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
