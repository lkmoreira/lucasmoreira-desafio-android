package br.com.gitlist.utils;

import android.content.Context;
import android.os.Build;

/**
 /* Created by lucas.moreira on 15/09/16.
 *
 */
public class Utilities {

    public static int getColorWrapper(Context context, int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.getColor(id);
        } else {
            return context.getResources().getColor(id);
        }
    }
}
