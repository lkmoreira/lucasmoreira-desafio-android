package br.com.gitlist.business.interactor;

import br.com.gitlist.business.interactor.listener.OnLoadRepositoryPullDetailListener;

/**
 * Created by lukas on 26/11/16.
 */

public interface RepositoryPullDetailInteractor {

    void findRepositoriesDetails(OnLoadRepositoryPullDetailListener listener, boolean isUpdate, String creatorName, String repositoryName);

}
