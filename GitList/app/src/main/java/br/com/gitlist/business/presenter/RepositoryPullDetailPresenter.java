package br.com.gitlist.business.presenter;

import br.com.gitlist.business.view.RepositoryPullDetailView;

/**
 * Created by lucas.moreira on 26/11/16.
 *
 */

public interface RepositoryPullDetailPresenter {

    void setView(RepositoryPullDetailView view);

    void findRepositoriesDetails(boolean isUpdate, String creatorName, String repositoryName);

}
