package br.com.gitlist.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

import br.com.gitlist.business.activity.base.BaseActivity;
import br.com.gitlist.business.model.Repository;
import br.com.gitlist.business.model.RepositoryPullDetail;
import br.com.gitlist.ui.customview.RepositoryPullDetailItemView;
import br.com.gitlist.ui.customview.RepositoryPullDetailItemView_;


/**
 * Created by lucas.moreira on 26/11/16.
 *
 */
@EBean
public class RepositoryPullDetailsAdapter extends RecyclerView.Adapter<RepositoryPullDetailsAdapter.SimplePostHolder> {

    @RootContext
    protected BaseActivity activity;

    protected List<RepositoryPullDetail> repositoryPullDetails = new ArrayList<>();

    @Override
    public SimplePostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SimplePostHolder(RepositoryPullDetailItemView_.build(activity));
    }

    @Override
    public void onBindViewHolder(SimplePostHolder holder, int position) {
        final RepositoryPullDetail repositoryPullDetail = getItem(position);

        RepositoryPullDetailItemView repositoryItemView = ((RepositoryPullDetailItemView) holder.itemView).bind(repositoryPullDetail);

    }

    @Override
    public int getItemCount() {
        return repositoryPullDetails.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public RepositoryPullDetail getItem(int position) {
        return repositoryPullDetails.get(position);
    }

    public void setData(List<RepositoryPullDetail> pulls) {
        this.repositoryPullDetails.clear();
        this.repositoryPullDetails.addAll(pulls);
    }

    public void setData(List<RepositoryPullDetail> pulls, boolean isUpdate) {
        if (!isUpdate) {
            setData(pulls);
            notifyDataSetChanged();
            return;
        }
        this.repositoryPullDetails.addAll(pulls);
        notifyDataSetChanged();
    }

    public static class SimplePostHolder extends RecyclerView.ViewHolder {
        public SimplePostHolder(View v) {
            super(v);
        }
    }

}
