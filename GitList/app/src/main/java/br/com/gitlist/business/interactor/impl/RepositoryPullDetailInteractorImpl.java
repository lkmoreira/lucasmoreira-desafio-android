package br.com.gitlist.business.interactor.impl;

import org.androidannotations.annotations.EBean;

import java.util.List;

import br.com.gitlist.business.interactor.RepositoryPullDetailInteractor;
import br.com.gitlist.business.interactor.base.BaseInteractor;
import br.com.gitlist.business.interactor.listener.OnLoadRepositoryPullDetailListener;
import br.com.gitlist.business.model.RepositoryPullDetail;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by lucas.moreira on 26/11/16.
 *
 */
@EBean
public class RepositoryPullDetailInteractorImpl extends BaseInteractor implements RepositoryPullDetailInteractor {

    @Override
    public void findRepositoriesDetails(final OnLoadRepositoryPullDetailListener listener, final boolean isUpdate,
                                        String creatorName, String repositoryName) {
        Observable<List<RepositoryPullDetail>> repositoriesService = rest.getApi().getRepositoryPulls(creatorName, repositoryName);

        repositoriesService.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<RepositoryPullDetail>>() {
                    @Override
                    public void call(List<RepositoryPullDetail> repositories) {
                        listener.onSuccess(repositories, isUpdate);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        listener.onError();
                    }
                });
    }
}
