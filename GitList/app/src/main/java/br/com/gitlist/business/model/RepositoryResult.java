package br.com.gitlist.business.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucas.moreira on 26/11/16.
 *
 */

public class RepositoryResult implements Serializable {

    @SerializedName("total_count")
    @Expose
    public Integer totalCount;

    @SerializedName("incomplete_results")
    @Expose
    public Boolean incompleteResults;

    @SerializedName("items")
    @Expose
    public List<Repository> repositories = new ArrayList<Repository>();

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Boolean getIncompleteResults() {
        return incompleteResults;
    }

    public void setIncompleteResults(Boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    public List<Repository> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }
}
