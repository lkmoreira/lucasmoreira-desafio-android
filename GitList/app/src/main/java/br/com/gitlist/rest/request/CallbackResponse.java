package br.com.gitlist.rest.request;

/**
 * Created by lucas.moreira on 27/11/16.
 *
 */
public interface CallbackResponse<T> {

    public void onSuccess(T value);

    public void onError(Throwable throwable);
}

