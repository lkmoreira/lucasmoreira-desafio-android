package br.com.gitlist.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

import br.com.gitlist.business.activity.base.BaseActivity;
import br.com.gitlist.business.model.Repository;
import br.com.gitlist.ui.customview.RepositoryItemView;
import br.com.gitlist.ui.customview.RepositoryItemView_;


/**
 * Created by lucas.moreira on 26/11/16.
 *
 */
@EBean
public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.SimplePostHolder> {

    @RootContext
    protected BaseActivity activity;

    protected List<Repository> repositories = new ArrayList<>();

    @Override
    public SimplePostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SimplePostHolder(RepositoryItemView_.build(activity));
    }

    @Override
    public void onBindViewHolder(SimplePostHolder holder, int position) {
        final Repository repository = getItem(position);

        RepositoryItemView repositoryItemView = ((RepositoryItemView) holder.itemView).bind(repository);

    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public Repository getItem(int position) {
        return repositories.get(position);
    }

    public void setData(List<Repository> posts) {
        this.repositories.clear();
        this.repositories.addAll(posts);
    }

    public void setData(List<Repository> posts, boolean isUpdate) {
        if (!isUpdate) {
            setData(posts);
            notifyDataSetChanged();
            return;
        }
        this.repositories.addAll(posts);
        notifyDataSetChanged();
    }

    public static class SimplePostHolder extends RecyclerView.ViewHolder {
        public SimplePostHolder(View v) {
            super(v);
        }
    }

}
