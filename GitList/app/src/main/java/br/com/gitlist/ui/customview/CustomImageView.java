package br.com.gitlist.ui.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.gitlist.R;
import br.com.gitlist.utils.VolleySingleton;


/**
 * Created by lucas.moreira on 27/11/16.
 *
 */
@EViewGroup(R.layout.custom_image_view)
public class CustomImageView extends RelativeLayout {

    @ViewById
    protected CustomNetworkImageView imageSource;
    @ViewById
    protected ProgressBar imageProgressBar;

    public CustomImageView(Context context) {
        super(context);
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @AfterViews
    void afterViews() {
        imageProgressBar.setVisibility(View.VISIBLE);
    }

    public void displayImage(final String uri, Context context) {
        Log.i("Volley", "URL: " + uri);
        if (null != uri && !uri.isEmpty() && uri.startsWith("http")) {
            VolleySingleton.loadImage(uri, imageSource, context);
        } else {
            this.setVisibility(GONE);
        }
    }

}
