package br.com.gitlist.business.presenter;

import br.com.gitlist.business.view.RepositoryView;

/**
 * Created by lucas.moreira on 26/11/16.
 *
 */

public interface RepositoryPresenter {

    void setView(RepositoryView view);

    void findRepositories(int page, boolean isUpdate);

}
