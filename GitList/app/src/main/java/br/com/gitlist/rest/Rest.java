package br.com.gitlist.rest;

import android.content.Context;
import android.net.Uri;

import com.google.gson.GsonBuilder;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.res.StringRes;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import br.com.gitlist.R;
import br.com.gitlist.rest.interceptor.LoggingInterceptor;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import retrofit2.RxJavaCallAdapterFactory;

/**
 * Created by lukas on 26/11/16.
 */

@EBean(scope = EBean.Scope.Singleton)
public class Rest {

    @RootContext
    protected Context context;

    @StringRes(R.string.base_url)
    protected String baseUrl;

    private Api api;

    @AfterInject
    void afterInjection() {
        buildApi();
    }

    public void buildApi() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addNetworkInterceptor(new LoggingInterceptor())
                .cache(new Cache(context.getCacheDir(), 10 * 1024 * 1024))
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();

        Retrofit retrofitController = new Retrofit.Builder()
                .client(client)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .excludeFieldsWithoutExposeAnnotation()
                        .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                        .create()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .callbackExecutor(Executors.newCachedThreadPool())
                .build();


        api = retrofitController.create(Api.class);
    }

    public Api getApi() {
        return api;
    }
}
