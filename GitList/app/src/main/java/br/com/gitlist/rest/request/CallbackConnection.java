package br.com.gitlist.rest.request;

/**
 * Created by lucas.moreira on 27/11/16.
 *
 */
public interface CallbackConnection {
    public void onConnection();

    public void onOffline();
}
