package br.com.gitlist.ui.customview;

import android.content.Context;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.gitlist.R;
import br.com.gitlist.business.activity.base.BaseActivity;
import br.com.gitlist.business.model.Repository;
import br.com.gitlist.business.model.RepositoryPullDetail;
import br.com.gitlist.utils.Utilities;


/**
 * Created by lucas.moreira on 26/11/16.
 */
@EViewGroup(R.layout.repository_pull_item)
public class RepositoryPullDetailItemView extends LinearLayout {

    @ViewById
    protected TextView pullName, pullDescription, username, fullName;

    @ViewById
    protected CustomCircleImageView thumbnail;

    private BaseActivity activity;
    private RepositoryPullDetail repositoryPullDetail;

    public RepositoryPullDetailItemView(Context context) {
        super(context);
        activity = (BaseActivity) context;
    }

    public RepositoryPullDetailItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        activity = (BaseActivity) context;
    }

    public RepositoryPullDetailItemView bind(final RepositoryPullDetail repositoryPullDetail) {
        this.repositoryPullDetail = repositoryPullDetail;

        pullName.setText(repositoryPullDetail.getTitle());
        pullDescription.setText(repositoryPullDetail.getBody());
        username.setText(repositoryPullDetail.getUser().getLogin());

        thumbnail.displayImage(repositoryPullDetail.getUser().getAvatarUrl(), activity, R.drawable.account_circle);

        return this;
    }

    @Click(R.id.repository_item)
    void showDetails() {
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(Utilities.getColorWrapper(activity, R.color.colorPrimary));

        builder.setStartAnimations(activity, android.R.anim.fade_in, android.R.anim.fade_out);
        builder.setExitAnimations(activity, android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(activity, Uri.parse(repositoryPullDetail.getHtmlUrl()));
    }


}
