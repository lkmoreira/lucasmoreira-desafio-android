package br.com.gitlist.business.presenter.impl;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import br.com.gitlist.business.interactor.RepositoryPullDetailInteractor;
import br.com.gitlist.business.interactor.impl.RepositoryPullDetailInteractorImpl;
import br.com.gitlist.business.interactor.listener.OnLoadRepositoryListener;
import br.com.gitlist.business.interactor.listener.OnLoadRepositoryPullDetailListener;
import br.com.gitlist.business.model.Repository;
import br.com.gitlist.business.model.RepositoryPullDetail;
import br.com.gitlist.business.presenter.RepositoryPullDetailPresenter;
import br.com.gitlist.business.view.RepositoryPullDetailView;
import br.com.gitlist.business.view.RepositoryView;

/**
 * Created by lucas.moreira on 26/11/16.
 *
 */
@EBean
public class RepositoryPullDetailPresenterImpl implements RepositoryPullDetailPresenter, OnLoadRepositoryPullDetailListener {


    private RepositoryPullDetailView repositoryPullDetailView;

    @Bean(RepositoryPullDetailInteractorImpl.class)
    protected RepositoryPullDetailInteractor repositoryPullDetailInteractor;

    @Override
    public void onSuccess() {

    }

    @Override
    public void onSuccess(List<RepositoryPullDetail> repositories, boolean isUpdate) {
        repositoryPullDetailView.populateList(repositories, repositories.size() > 0);
    }

    @Override
    public void onError() {
        repositoryPullDetailView.populateList(new ArrayList<RepositoryPullDetail>(), false);
    }

    @Override
    public void findRepositoriesDetails(boolean isUpdate, String creatorName, String repositoryName) {
        repositoryPullDetailInteractor.findRepositoriesDetails(this, isUpdate, creatorName, repositoryName);
    }

    @Override
    public void setView(RepositoryPullDetailView view) {
        this.repositoryPullDetailView = view;
    }
}
