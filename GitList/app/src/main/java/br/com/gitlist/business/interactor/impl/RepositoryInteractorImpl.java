package br.com.gitlist.business.interactor.impl;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import br.com.gitlist.business.interactor.RepositoryInteractor;
import br.com.gitlist.business.interactor.base.BaseInteractor;
import br.com.gitlist.business.interactor.listener.OnLoadRepositoryListener;
import br.com.gitlist.business.model.Repository;
import br.com.gitlist.business.model.RepositoryResult;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by lucas.moreira on 26/11/16.
 *
 */
@EBean
public class RepositoryInteractorImpl extends BaseInteractor implements RepositoryInteractor {

    @Override
    public void findRepositories(final OnLoadRepositoryListener listener, int page, final boolean isUpdate) {
        Observable<RepositoryResult> repositoriesService = rest.getApi().getRepositories(page);

        repositoriesService.map(new Func1<RepositoryResult, List<Repository>>() {

            @Override
            public List<Repository> call(RepositoryResult repositoryResult) {
                return repositoryResult.getRepositories();
            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<Repository>>() {
                    @Override
                    public void call(List<Repository> repositories) {
                        listener.onSuccess(repositories, isUpdate);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        listener.onError();
                    }
                });
    }
}
