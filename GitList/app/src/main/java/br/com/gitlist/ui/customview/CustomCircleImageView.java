package br.com.gitlist.ui.customview;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.android.volley.toolbox.ImageLoader;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.gitlist.R;
import br.com.gitlist.utils.VolleySingleton;


/**
 * Created by lucas.moreira on 12/09/16.
 */
@EViewGroup(R.layout.custom_circle_image_view)
public class CustomCircleImageView extends RelativeLayout {

    @ViewById
    protected CircleNetworkImageView circleImageSource;
    @ViewById
    protected ProgressBar imageProgressBar;

    public CustomCircleImageView(Context context) {
        super(context);
    }

    public CustomCircleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @AfterViews
    void afterViews() {
        imageProgressBar.setVisibility(View.VISIBLE);
        circleImageSource.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                imageProgressBar.setVisibility(GONE);

            }
        });
    }

    public void displayImage(final String uri, Context context) {
        Log.i("Volley", "URL: " + uri);
        VolleySingleton.loadImage(uri, circleImageSource, context);
    }

    public void displayImage(final String uri, Context context, int errorImageResId) {
        Log.i("Volley", "URL: " + uri);
        VolleySingleton.loadImage(uri, circleImageSource, context, errorImageResId);
    }

}
