package br.com.gitlist.business.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NonConfigurationInstance;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.gitlist.R;
import br.com.gitlist.business.activity.base.BaseActivity;
import br.com.gitlist.business.model.Repository;
import br.com.gitlist.business.model.RepositoryResult;
import br.com.gitlist.business.presenter.RepositoryPresenter;
import br.com.gitlist.business.presenter.impl.RepositoryPresenterImpl;
import br.com.gitlist.business.view.RepositoryView;
import br.com.gitlist.rest.Rest;
import br.com.gitlist.ui.adapter.RepositoryAdapter;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

@EActivity(R.layout.activity_repository)
public class RepositoryActivity extends BaseActivity implements RepositoryView {

    @ViewById
    protected RecyclerView items;

    @ViewById
    protected SwipeRefreshLayout swipeRefreshContainer;

    @ViewById
    protected RelativeLayout emptyView;

    @Bean(RepositoryPresenterImpl.class)
    protected RepositoryPresenter presenter;

    @Bean
    @NonConfigurationInstance
    protected RepositoryAdapter repositoryAdapter;

    protected int page = 1;
    protected boolean isLoading, hasMore = true;

    @AfterInject
    protected void afterInject() {
        checkIfHasInternet(true);
    }

    @Override
    protected void afterViews() {
        setupRecyclerView();
        showProgress();

        presenter.setView(this);
        presenter.findRepositories(page, false);

        swipeRefreshContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadItems(true);
            }
        });
    }

    @Override
    public void setupRecyclerView() {
        if (items == null)
            return;

        final GridLayoutManager layoutManager = new GridLayoutManager(this, this.getResources().getInteger(R.integer.num_of_columns));
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.scrollToPosition(0);
        items.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        items.setAdapter(repositoryAdapter);
        items.setLayoutManager(layoutManager);

        items.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                int visibleItemCount = layoutManager.getChildCount();

                if (((visibleItemCount + firstVisibleItem) >= totalItemCount) && hasMore && !isLoading) {
                    isLoading = true;
                    loadMore();
                }
            }
        });
    }

    @Override
    public void populateList(List<Repository> value, boolean isUpdate) {
        if (value.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            items.setVisibility(View.GONE);
            return;
        } else {
            items.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }

        repositoryAdapter.setData(value, isUpdate);

        if (value.size() == 0)
            hasMore = false;
        else
            page++;

        isLoading = false;

        if (swipeRefreshContainer.isRefreshing())
            swipeRefreshContainer.setRefreshing(false);

        hideProgress();
    }

    @Override
    public void loadMore() {
        if(hasNetworkConnection) {
            showProgress();
            presenter.findRepositories(page, true);
        }
    }

    @Override
    public void loadItems(boolean isUpdate) {
        if(hasNetworkConnection) {
            showProgress();
            presenter.findRepositories(page, true);
        }
    }
}
