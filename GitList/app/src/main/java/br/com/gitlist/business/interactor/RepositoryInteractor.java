package br.com.gitlist.business.interactor;

import br.com.gitlist.business.interactor.listener.OnLoadRepositoryListener;

/**
 * Created by lukas on 26/11/16.
 */

public interface RepositoryInteractor {

    void findRepositories(OnLoadRepositoryListener listener, int page, boolean isUpdate);

}
