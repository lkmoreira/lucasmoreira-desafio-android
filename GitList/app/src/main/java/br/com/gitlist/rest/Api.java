package br.com.gitlist.rest;

import java.util.List;

import br.com.gitlist.business.model.RepositoryPullDetail;
import br.com.gitlist.business.model.RepositoryResult;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by lucas.moreira on 26/11/16.
 *
 */

public interface Api {

    @GET("search/repositories?q=language:Java&sort=stars")
    Observable<RepositoryResult> getRepositories(@Query("page") int page);

    @GET("repos/{creator}/{repository}/pulls")
    Observable<List<RepositoryPullDetail>> getRepositoryPulls(@Path("creator") String creator, @Path("repository") String repository);
}
