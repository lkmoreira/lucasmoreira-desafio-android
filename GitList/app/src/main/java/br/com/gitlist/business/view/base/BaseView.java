package br.com.gitlist.business.view.base;

/**
 * Created by lucas.moreira on 26/11/16.
 *
 */

public interface BaseView {

    void showProgress();

    void hideProgress();

    void showOfflineProgress();

    void hideOfflineProgress();

}
