package br.com.gitlist.business.interactor.listener;

import java.util.List;

import br.com.gitlist.business.model.Repository;

/**
 * Created by lukas on 26/11/16.
 */

public interface OnLoadRepositoryListener {

    void onSuccess();

    void onSuccess(List<Repository> repositories, boolean isUpdate);

    void onError();
}
